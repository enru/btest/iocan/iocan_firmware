#include "outputs.h"
#include "main.h"

void out1_set(void) {
    HAL_GPIO_WritePin(KEY1_GPIO_Port, KEY1_Pin, GPIO_PIN_SET);
}

void out1_reset(void) {
    HAL_GPIO_WritePin(KEY1_GPIO_Port, KEY1_Pin, GPIO_PIN_RESET);
}

void out2_set(void) {
    HAL_GPIO_WritePin(KEY2_GPIO_Port, KEY2_Pin, GPIO_PIN_SET);
}

void out2_reset(void) {
    HAL_GPIO_WritePin(KEY2_GPIO_Port, KEY2_Pin, GPIO_PIN_RESET);
}

void out3_set(void) {
    HAL_GPIO_WritePin(KEY3_GPIO_Port, KEY3_Pin, GPIO_PIN_SET);
}

void out3_reset(void) {
    HAL_GPIO_WritePin(KEY3_GPIO_Port, KEY3_Pin, GPIO_PIN_RESET);
}

void out4_set(void) {
    HAL_GPIO_WritePin(KEY4_GPIO_Port, KEY4_Pin, GPIO_PIN_SET);
}

void out4_reset(void) {
    HAL_GPIO_WritePin(KEY4_GPIO_Port, KEY4_Pin, GPIO_PIN_RESET);
}

void out5_set(void) {
    HAL_GPIO_WritePin(KEY5_GPIO_Port, KEY5_Pin, GPIO_PIN_SET);
}

void out5_reset(void) {
    HAL_GPIO_WritePin(KEY5_GPIO_Port, KEY5_Pin, GPIO_PIN_RESET);
}

void outs_set(uint8_t state) {
    HAL_GPIO_WritePin(KEY1_GPIO_Port, KEY1_Pin, state & 0x01);
    HAL_GPIO_WritePin(KEY2_GPIO_Port, KEY2_Pin, (state & 0x02) >> 1);
    HAL_GPIO_WritePin(KEY3_GPIO_Port, KEY3_Pin, (state & 0x04) >> 2);
    HAL_GPIO_WritePin(KEY4_GPIO_Port, KEY4_Pin, (state & 0x08) >> 3);
    HAL_GPIO_WritePin(KEY5_GPIO_Port, KEY5_Pin, (state & 0x10) >> 4);
}