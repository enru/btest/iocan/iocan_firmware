#include "rcan.h"
#include "led.h"
#include "outputs.h"
#include "main.h"

#include "stm32g474xx.h"
#include "inputs.h"


#define CAN_BITRATE        1000000
#define CAN_IFACE          FDCAN1_BASE
#define OUTPUT_SET_ID      100
#define INPUT_STATE_ID     200

rcan can;
rcan_frame receive_frame = {0};
rcan_frame send_frame = {.id = INPUT_STATE_ID, .type = std_id, .len = 1, .rtr = false, .payload = {0}};


void iocan_init(void) {
    rcan_start(&can, CAN_IFACE, CAN_BITRATE);
}

void iocan_rx_loop(void) {

    if (rcan_receive(&can, &receive_frame)) {
        if (receive_frame.id == OUTPUT_SET_ID) {
            outs_set(receive_frame.payload[0]);
        }
    }
}


void iocan_tx_loop(void) {

    if (!rcan_is_ok(&can)) {
        led_red_on();
        rcan_stop(&can);
        HAL_Delay(100);
        rcan_start(&can, CAN_IFACE, CAN_BITRATE);
        led_red_off();
    }

    send_frame.payload[0] = inputs_get_current_state();
    if (!rcan_send(&can, &send_frame))
        led_red_on();
    else
        led_red_off();


}