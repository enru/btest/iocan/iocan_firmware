#ifndef  __INPUTS_H
#define  __INPUTS_H

#include "stdbool.h"
#include "stdint.h"

bool input_1_is_set(void);

bool input_2_is_set(void);

bool input_3_is_set(void);

bool input_4_is_set(void);

bool input_5_is_set(void);

uint8_t inputs_get_current_state(void);

#endif