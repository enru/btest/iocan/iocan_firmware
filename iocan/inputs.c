#include "inputs.h"
#include "main.h"

typedef bool (*inputs)(void);

bool input_1_is_set(void) {
    return HAL_GPIO_ReadPin(INPUT1_GPIO_Port, INPUT1_Pin) == GPIO_PIN_SET;
}

bool input_2_is_set(void) {
    return HAL_GPIO_ReadPin(INPUT2_GPIO_Port, INPUT2_Pin) == GPIO_PIN_SET;
}

bool input_3_is_set(void) {
    return HAL_GPIO_ReadPin(INPUT3_GPIO_Port, INPUT3_Pin) == GPIO_PIN_SET;
}

bool input_4_is_set(void) {
    return HAL_GPIO_ReadPin(INPUT4_GPIO_Port, INPUT4_Pin) == GPIO_PIN_SET;
}

bool input_5_is_set(void) {
    return HAL_GPIO_ReadPin(INPUT5_GPIO_Port, INPUT5_Pin) == GPIO_PIN_SET;
}

void write_state_to_variable_from_input_function(uint8_t *variable, uint8_t bit_pos, inputs func) {
    if (func())
        *variable &= ~(1 << bit_pos);
    else
        *variable |= (1 << bit_pos);
}

uint8_t inputs_get_current_state(void) {

    uint8_t inputs_state = 0;
    write_state_to_variable_from_input_function(&inputs_state, 0, input_1_is_set);
    write_state_to_variable_from_input_function(&inputs_state, 1, input_2_is_set);
    write_state_to_variable_from_input_function(&inputs_state, 2, input_3_is_set);
    write_state_to_variable_from_input_function(&inputs_state, 3, input_4_is_set);
    write_state_to_variable_from_input_function(&inputs_state, 4, input_5_is_set);
    return inputs_state;
}
