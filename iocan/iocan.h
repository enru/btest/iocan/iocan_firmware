#ifndef __IOCAN_H
#define __IOCAN_H


void iocan_init(void);

void iocan_rx_loop(void);

void iocan_tx_loop(void);

#endif