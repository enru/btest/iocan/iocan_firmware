#ifndef __OUTPUTS_H
#define __OUTPUTS_H

#include "stdint.h"

void out1_set(void);

void out1_reset(void);

void out2_set(void);

void out2_reset(void);

void out3_set(void);

void out3_reset(void);

void out4_set(void);

void out4_reset(void);

void out5_set(void);

void out5_reset(void);

void outs_set(uint8_t state);

#endif