/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : app_freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"


/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdio.h"
#include "iocan.h"
#include "stdbool.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */
bool stated = false;
/* USER CODE END Variables */
osThreadId proto_taskHandle;
uint32_t proto_task_buffer[ 1024 ];
osStaticThreadDef_t proto_task_control_block;
osThreadId console_taskHandle;
uint32_t console_task_buffer[ 1024 ];
osStaticThreadDef_t console_task_block;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */
//uint8_t data[1024 * 1];
/* USER CODE END FunctionPrototypes */

void start_proto_task(void const * argument);
void start_console_task(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* GetIdleTaskMemory prototype (linked to static allocation support) */
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize );

/* USER CODE BEGIN GET_IDLE_TASK_MEMORY */
static StaticTask_t xIdleTaskTCBBuffer;
static StackType_t xIdleStack[configMINIMAL_STACK_SIZE];

void vApplicationGetIdleTaskMemory(StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer,
                                   uint32_t *pulIdleTaskStackSize) {
    *ppxIdleTaskTCBBuffer = &xIdleTaskTCBBuffer;
    *ppxIdleTaskStackBuffer = &xIdleStack[0];
    *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
    /* place for user code */
}
/* USER CODE END GET_IDLE_TASK_MEMORY */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
    /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
    /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
    /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
    /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of proto_task */
  osThreadStaticDef(proto_task, start_proto_task, osPriorityAboveNormal, 0, 1024, proto_task_buffer, &proto_task_control_block);
  proto_taskHandle = osThreadCreate(osThread(proto_task), NULL);

  /* definition and creation of console_task */
  osThreadStaticDef(console_task, start_console_task, osPriorityNormal, 0, 1024, console_task_buffer, &console_task_block);
  console_taskHandle = osThreadCreate(osThread(console_task), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
    /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_start_proto_task */
/**
  * @brief  Function implementing the proto_task thread.
  * @param  argument: Not used 
  * @retval None
  */
/* USER CODE END Header_start_proto_task */
void start_proto_task(void const * argument)
{
  /* USER CODE BEGIN start_proto_task */
    iocan_init();
    stated = true;
    /* Infinite loop */
    for (;;) {
        vTaskDelay(pdMS_TO_TICKS(1));
        iocan_rx_loop();
    }
  /* USER CODE END start_proto_task */
}

/* USER CODE BEGIN Header_start_console_task */
/**
* @brief Function implementing the console_task thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_start_console_task */
void start_console_task(void const * argument)
{
  /* USER CODE BEGIN start_console_task */
    /* Infinite loop */
    while(!stated){
        HAL_Delay(3);
    }
    for (;;) {
        vTaskDelay(pdMS_TO_TICKS(1));
        iocan_tx_loop();
    }

  /* USER CODE END start_console_task */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
